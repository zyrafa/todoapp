﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using ToDoApi.Models;
using ToDoApi.Repositories;

namespace ToDoApi.Controllers
{
    public class ToDoController : ApiController
    {
        private readonly IToDoRepository toDoRepository;

        public ToDoController(IToDoRepository toDoRepository)
        {
            this.toDoRepository = toDoRepository;
        }

        [HttpGet]
        public IEnumerable<ToDoItem> FindAll()
        {
            return toDoRepository.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            var toDoItem = toDoRepository.GetById(id);
            if (toDoItem == null)
                return NotFound();

            return Ok(toDoItem);
        }

        public void Post([FromBody]ToDoCreateRequest createRequest)
        {
            toDoRepository.Add(createRequest.Name);
        }

        public void Delete(int id)
        {
            toDoRepository.Delete(id);
        }

        [HttpPatch]
        [Route("api/todo/{id}/done")]
        public void Patch(int id)
        {
            var toDoItem = toDoRepository.GetById(id);
            toDoRepository.Save(toDoItem);
        }
    }
}