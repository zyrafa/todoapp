﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoApi.Controllers
{
    public class ToDoCreateRequest
    {
        public string Name { get; set; }
    }
}