﻿const uri = 'api/todo';

function handleError() {
    alert('Something went wrong :(');
}

function appendRow(item, checked) {
    $('<tr><td><input type="checkbox" ' + checked + ' onclick=markDone(' + item.id + ')></td>' +
        '<td>' + item.name + '</td>' +
        '<td><button class="btn btn-default" onclick="deleteItem(' + item.id + ')"><span class="glyphicon glyphicon-remove"></span></button></td>' +
        '</tr>').appendTo($('#todos'));
}

function getData() {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:51701/api/todo',
        success: function (data) {
            $('#todos').empty();
            $.each(data, function (key, item) {
                const checked = item.isCompleted ? 'checked' : '';
                appendRow(item, checked);
            });

            todos = data;
        },
        error: handleError
    });
}

$(document).ready(function () {
    getData();
});

function addItem() {
    const item = {
        'name': $('#to-do-name').val()
    };

    $.ajax({
        type: 'POST',
        accepts: 'application/json',
        url: uri,
        contentType: 'application/json',
        data: JSON.stringify(item),
        success: function (result) {
            //getData();
            $('#add-name').val('');
        },
        error: handleError
    });
}


function markDone(id) {
    $.ajax({
        type: 'PATCH',
        url: 'http://localhost:51701/api/todo/' + id +'/done',
        contentType: 'application/json',
        success: function (result) {
            getData();
            alert('You did it!');
        },
        error: handleError
    });
}

function deleteItem(id) {
    $.ajax({
        type: 'DELETE',
        accepts: 'application/json',
        url: uri + '/' + id,
        contentType: 'application/json',
        success: function (result) {
            getData();
            $('#add-name').val('');
        },
        error: handleError
    });
}