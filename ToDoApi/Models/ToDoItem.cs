﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoApi.Models
{
    public class ToDoItem
    {
        private bool _isCompleted = false;
        public int Id { get; }
        public string Name { get; }
        public bool IsCompleted
            => _isCompleted;

        public ToDoItem(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public void MarkAsCompleted()
        {
            _isCompleted = true;
        }
    }
}