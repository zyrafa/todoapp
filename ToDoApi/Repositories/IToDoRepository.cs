﻿using System.Collections.Generic;
using ToDoApi.Models;

namespace ToDoApi.Repositories
{
    public interface IToDoRepository
    {
        IEnumerable<ToDoItem> GetAll();
        ToDoItem GetById(int id);
        void Add(string name);
        void Delete(int id);
        void Save(ToDoItem toDoItem);
    }
}