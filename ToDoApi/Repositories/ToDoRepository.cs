﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoApi.Models;

namespace ToDoApi.Repositories
{
    public class ToDoRepository : IToDoRepository
    {
        private readonly IList<ToDoItem> storedItems = new List<ToDoItem>
        {
            new ToDoItem(1, "Wash the dishes"),
            new ToDoItem(2, "Go jogging"),
            new ToDoItem(3, "Lay down and rest")
        };

        private int GetNextId()
        {
            return storedItems.Last().Id + 1;
        }


        public void Add(string name)
        {
            storedItems.Add(new ToDoItem(GetNextId(), name));
        }

        public IEnumerable<ToDoItem> GetAll()
        {
            return storedItems;
        }

        public ToDoItem GetById(int id)
        {
            return storedItems.FirstOrDefault(si => si.Id == id);
        }

        public void Delete(int id)
        {
            var item = GetById(id);
            if (item != null)
                storedItems.Remove(item);
        }

        public void Save(ToDoItem toDoItem)
        {
            var existingItem = GetById(toDoItem.Id);
            if(existingItem != null)
            {
                existingItem.MarkAsCompleted();
            }
        }
    }
}